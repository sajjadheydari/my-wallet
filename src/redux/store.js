import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";

const connectedNetwork = localStorage.getItem("connectedNetwork")
  ? JSON.parse(localStorage.getItem("connectedNetwork"))
  : [];

const walletAddress = localStorage.getItem("walletAddress")
  ? JSON.parse(localStorage.getItem("walletAddress"))
  : [];

const initialState = {
  wallet: {
    connectedNetwork,
    walletAddress,
    balance: [],
    hashKey: {fundHash: [], transferHash: []},
    loading: false,
  },
  messages: {
    connectNetwork: [],
    createAccount: [],
    fundAccount: [],
    getBalance: [],
    transfer: [],
  },
};

const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

export default store;
