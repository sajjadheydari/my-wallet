import axios from "axios";
import {
  CONNECT_NETWORK,
  GENERATE_KEYPAI,
  FUND_ACCOUNT,
  STOP_LOADING,
  GET_BALANCE,
  TRANSFER_SOL,
  CONNECT_NETWORK_MESSAGE,
  GENERATE_KEYPAIR_MESSAGE,
  GET_BALANCE_MESSAGE,
  TRANSFER_SOL_MESSAGE,
  FUND_ACCOUNT_MESSAGE,
  START_LOADING,
} from "./constants";

// Connect to blockchain network
export const connectNetwork = (network) => (dispatch, getState) => {
  dispatch({ type: START_LOADING });
  axios
    .post("http://localhost:8000/", {
      network,
    })
    .then((response) => {
      dispatch({
        type: CONNECT_NETWORK,
        payload: response.data,
      });
      dispatch({
        type: CONNECT_NETWORK_MESSAGE,
        payload: { id: 1, text: "Successfully connected to Network!" },
      });
      dispatch({ type: STOP_LOADING });
      localStorage.setItem(
        "connectedNetwork",
        JSON.stringify(getState().wallet.connectedNetwork)
      );
    })
    .catch((error) => {
      dispatch({ type: STOP_LOADING });
      dispatch({
        type: CONNECT_NETWORK_MESSAGE,
        payload: { is: 2, text: "Network connection faild!" },
      });
    });
};

// Generate a keypair
export const generateKeypair = () => (dispatch, getState) => {
  dispatch({ type: START_LOADING });
  axios
    .post("http://localhost:8000/generate")
    .then((response) => {
      dispatch({
        type: GENERATE_KEYPAI,
        payload: response.data,
      });
      dispatch({
        type: GENERATE_KEYPAIR_MESSAGE,
        payload: { id: 1, text: "Successfully created account!" },
      });
      dispatch({ type: STOP_LOADING });
      localStorage.setItem(
        "walletAddress",
        JSON.stringify(getState().wallet.walletAddress)
      );
    })
    .catch((error) => {
      console.log(error);
      dispatch({ type: STOP_LOADING });
      dispatch({
        type: GENERATE_KEYPAIR_MESSAGE,
        payload: { id: 2, text: "Faild to create account!" },
      });
    });
};

//Fund your account
export const fundAccount = (network, address, SOL) => (dispatch, getState) => {
  dispatch({ type: START_LOADING });
  axios
    .post("http://localhost:8000/fund", {
      network,
      address,
      SOL,
    })
    .then((response) => {
      dispatch({
        type: FUND_ACCOUNT,
        payload: response.data,
      });
      dispatch({
        type: FUND_ACCOUNT_MESSAGE,
        payload: { id: 1, text: "Amount added to your account successfully!" },
      });
      dispatch({ type: STOP_LOADING });
    })
    .catch((error) => {
      console.log(error);
      dispatch({ type: STOP_LOADING });
      dispatch({
        type: FUND_ACCOUNT_MESSAGE,
        payload: { id: 2, text: "Failed to fund account!" },
      });
    });
};

//Get balance
export const getBalance = (network, address) => (dispatch, getState) => {
  dispatch({ type: START_LOADING });
  axios
    .post("http://localhost:8000/balance", {
      network,
      address,
    })
    .then((response) => {
      dispatch({
        type: GET_BALANCE,
        payload: response.data,
      });
      dispatch({
        type: GET_BALANCE_MESSAGE,
        payload: "",
      });
      dispatch({ type: STOP_LOADING });
    })
    .catch((error) => {
      console.log(error.message);
      dispatch({ type: STOP_LOADING });
      dispatch({
        type: GET_BALANCE_MESSAGE,
        payload: { id: 2, text: "Failed to get balance!" },
      });
    });
};

//Transfer SOL
export const transfer =
  (address, secret, recipient, SOL, network) => (dispatch, getState) => {
    dispatch({ type: START_LOADING });
    axios
      .post("http://localhost:8000/transfer", {
        address,
        secret,
        recipient,
        SOL,
        network,
      })
      .then((response) => {
        dispatch({
          type: TRANSFER_SOL,
          payload: response.data,
        });
        dispatch({
          type: TRANSFER_SOL_MESSAGE,
          payload: { id: 1, text: "Amount successfully transfered!" },
        });
        dispatch({ type: STOP_LOADING });
      })
      .catch((error) => {
        console.log(error.response);
        dispatch({ type: STOP_LOADING });
        dispatch({
          type: TRANSFER_SOL_MESSAGE,
          payload: {
            id: 2,
            text:
              error.response && error.response.data
                ? error.response.data
                : error.message,
          },
        });
      });
  };
