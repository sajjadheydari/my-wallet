import { combineReducers } from "redux";
import WalletReducer from "./walletReducer";
import MessageReducer from "./messageReducer";

export default combineReducers({
  wallet: WalletReducer,
  messages: MessageReducer,
});
