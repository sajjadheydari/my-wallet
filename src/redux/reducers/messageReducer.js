import {
  CONNECT_NETWORK_MESSAGE,
  GENERATE_KEYPAIR_MESSAGE,
  FUND_ACCOUNT_MESSAGE,
  GET_BALANCE_MESSAGE,
  TRANSFER_SOL_MESSAGE,
} from "../actions/constants";

const initialState = {
  connectNetwork: [],
  createAccount: [],
  fundAccount: [],
  getBalance: [],
  transfer: [],
};

const MessageReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONNECT_NETWORK_MESSAGE:
      return {
        ...state,
        connectNetwork: action.payload,
      };
    case GENERATE_KEYPAIR_MESSAGE:
      return {
        ...state,
        createAccount: action.payload,
        connectNetwork: [],
        fundAccount: [],
        getBalance: [],
        transfer: [],
      };
    case FUND_ACCOUNT_MESSAGE:
      return {
        ...state,
        fundAccount: action.payload,
      };
    case GET_BALANCE_MESSAGE:
      return {
        ...state,
        getBalance: action.payload,
      };
    case TRANSFER_SOL_MESSAGE:
      return {
        ...state,
        transfer: action.payload,
      };

    default:
      return state;
  }
};

export default MessageReducer;
