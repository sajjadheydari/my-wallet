import {
  CONNECT_NETWORK,
  FUND_ACCOUNT,
  GENERATE_KEYPAI,
  GET_BALANCE,
  START_LOADING,
  STOP_LOADING,
  TRANSFER_SOL,
} from "../actions/constants";

const initialState = {
  connectedNetwork: null,
  walletAddress: null,
  balance: null,
  hashKey: { fundHash: [], transferHash: [] },
  loading: false,
};

const WalletReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONNECT_NETWORK:
      return {
        ...state,
        connectedNetwork: action.payload,
      };
    case GENERATE_KEYPAI:
      return {
        ...state,
        walletAddress: action.payload,
        balance: [],
        hashKey: [],
      };
    case FUND_ACCOUNT:
      return {
        ...state,
        hashKey: {
          ...state.hashKey,
          fundHash: action.payload,
        },
      };
    case GET_BALANCE:
      return {
        ...state,
        balance: action.payload,
      };
    case TRANSFER_SOL:
      return {
        ...state,
        hashKey: {
          ...state.hashKey,
          transferHash: action.payload,
        },
      };
    case START_LOADING:
      return {
        ...state,
        loading: true,
      };
    case STOP_LOADING:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export default WalletReducer;
