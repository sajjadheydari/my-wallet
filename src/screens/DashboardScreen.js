import React, { useState } from "react";
import SidebarComponent from "../components/SidebarComponent";
import WalletComponent from "../components/wallet/WalletComponent";
import { Grid } from "@mui/material";

const DashboardScreen = () => {
  const [isOpen, setIsOpen] = useState(true);

  const menuItems = [
    {
      title: "Home",
      path: "/",
      icon: <i className="fas fa-home"></i>,
    },
    {
      title: "Profile",
      path: "/profile",
      icon: <i className="fas fa-user-circle"></i>,
    },
  ];
  return (
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <SidebarComponent
          menuItems={menuItems}
          onToggle={() => setIsOpen(!isOpen)}
          isOpen={isOpen}
        />
        <WalletComponent isOpen={isOpen} />
      </Grid>
  );
};

export default DashboardScreen;
