import { Button, Grid, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import NavbarComponent from "../components/NavbarComponent";

const LandingScreen = () => {
  return (
    <Grid container className="landingscreen">
      <NavbarComponent />
      <Grid className="landingscreen-textbox">
        <Typography>Welcome to Your Crypto Wallet!</Typography>
        <Link to="/dashboard">
          <Button
            style={{ width: "fit-content" }}
            variant="contained"
            color="primary"
            type="submit"
            className="my-3"
          >
            Connect
          </Button>
        </Link>
      </Grid>
    </Grid>
  );
};

export default LandingScreen;
