import { Button, Grid, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import NotFoundImage from "../assets/images/404.svg";

const NotfoundScreen = () => {
  return (
    <Grid className="notfound-screen">
      <img
        src={NotFoundImage}
        alt="404"
        style={{ width: "50%", height: "auto" }}
      />
      <Typography className="notfound-screen-caption">Page Not Found 404!</Typography>
      <Link to="/">
        <Button
          style={{ width: "fit-content" }}
          variant="contained"
          color="primary"
          type="submit"
          className="my-3"
        >
          Back to Home
        </Button>
      </Link>
    </Grid>
  );
};

export default NotfoundScreen;
