import { FormControl } from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";

const FormBodyComponent = ({ children }) => {
  return <FormControl fullWidth>{children}</FormControl>;
};

FormBodyComponent.propTypes = {
  children: PropTypes.node.isRequired,
};

export default FormBodyComponent;
