import { Typography } from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";

const FormHeaderComponent = ({ title }) => {
  return (
    <Typography align="left" className="fundcomponent-title" gutterBottom>
      {title}
    </Typography>
  );
};

FormHeaderComponent.propTypes = {
  title: PropTypes.string,
};

export default FormHeaderComponent;
