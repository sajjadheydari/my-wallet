import { Alert, Button, CircularProgress, Grid } from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FormComponent from "../FormComponent";
import InputComponent from "../InputComponent";
import { transfer } from "../../redux/actions/walletActions";

const TransferComponent = () => {
  const [recipient, setRecipient] = useState("");
  const [SOL, setSOL] = useState([]);
  const { connectedNetwork, walletAddress, hashKey, loading } = useSelector(
    (state) => state.wallet
  );
  const messages = useSelector((state) => state.messages);
  const { address, secret } = walletAddress;
  const dispatch = useDispatch();

  const handleTransfer = (e) => {
    e.preventDefault();
    if (connectedNetwork === undefined || connectedNetwork.length === 0) {
      alert("Please connect to Network first!");
    } else if (address === undefined || address.length === 0) {
      alert("Please create/connect account first!");
    } else {
      dispatch(
        transfer(address, secret, recipient, SOL, connectedNetwork.network)
      );
    }
  };

  const validated =
    recipient === undefined ||
    recipient.length === 0 ||
    SOL === undefined ||
    SOL.length === 0
      ? true
      : false;

  return (
    <>
      {loading ? (
        <CircularProgress
          style={{ position: "fixed", top: "50%", left: "50%" }}
        />
      ) : (
        <Grid container className="d-flex justify-content-start">
          <Grid item xs={12} sm={11} md={8} lg={6}>
            <FormComponent
              title="Transfer Lamports to other accounts:"
              handleSubmit={handleTransfer}
            >
              {messages.transfer.length === 0 ||
              messages.transfer === undefined ? null : (
                <Alert
                  severity={messages.transfer.id === 1 ? "success" : "error"}
                  className={
                    messages.transfer.id === 1 ? "bg-success" : "bg-danger"
                  }
                >
                  {messages.transfer.text}
                </Alert>
              )}
              <InputComponent
                size="small"
                variant="outlined"
                label="From (My Address)"
                type="text"
                className="my-4 input-component"
                value={address}
                name="payer"
                inputProps={{
                  readOnly: true,
                }}
                InputLabelProps={{
                  shrink: true,
                  className: "input-label-props",
                }}
              />
              <InputComponent
                size="small"
                variant="outlined"
                label="To (Recipient Address)"
                type="text"
                className="my-4 input-component"
                value={recipient}
                name="recipient"
                onChange={setRecipient}
                InputLabelProps={{
                  shrink: true,
                  className: "input-label-props",
                }}
              />
              <InputComponent
                size="small"
                variant="outlined"
                label="Enter Lamports amount"
                type="number"
                className="my-4 input-component"
                value={SOL}
                name="sol"
                onChange={setSOL}
                InputLabelProps={{
                  shrink: true,
                  className: "input-label-props",
                }}
              />
              {hashKey.transferHash === undefined || hashKey.transferHash.length === 0 ? null : (
                <InputComponent
                  size="small"
                  variant="outlined"
                  label="Get your HashKey"
                  type="text"
                  className="my-4 input-component"
                  value={hashKey.transferHash}
                  name="transferHash"
                  inputProps={{
                    readOnly: true,
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: "input-label-props",
                  }}
                />
              )}
              <Button
                style={{ width: "fit-content" }}
                variant="contained"
                color="primary"
                type="submit"
                className="my-3"
                disabled={validated}
              >
                Transfer
              </Button>
            </FormComponent>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default TransferComponent;
