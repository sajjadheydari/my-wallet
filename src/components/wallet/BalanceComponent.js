import React from "react";
import { Button } from "@material-ui/core";
import { Alert } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getBalance } from "../../redux/actions/walletActions";
import FormComponent from "../FormComponent";

const BalanceComponent = () => {
  const { balance, connectedNetwork, walletAddress } = useSelector(
    (state) => state.wallet
  );
  const messages = useSelector((state) => state.messages);
  const { address } = walletAddress;
  const dispatch = useDispatch();

  const handleGetBalance = (e) => {
    e.preventDefault();
    if (connectedNetwork === undefined || connectedNetwork.length === 0) {
      alert("Please connect to Network first!");
    } else if (address === undefined || address.length === 0) {
      alert("Please create/connect account first!");
    } else {
      dispatch(getBalance(connectedNetwork.network, address));
    }
  };

  return (
    <FormComponent title="Check your balance:" handleSubmit={handleGetBalance} style={{height: 175}}>
      {messages.getBalance.length === 0 ||
      messages.getBalance === undefined ? null : (
        <Alert severity={messages.getBalance.id === 1 ? 'success' : 'error'} className={messages.getBalance.id === 1 ? 'bg-success' : 'bg-danger'}>
          {messages.getBalance.text}
        </Alert>
      )}
      {balance.length === 0 ? null : (
        <div className="mt-3">Your balance is: <b className='p-2 bg-success rounded text-white'>{balance}</b> lamports</div>
      )}
      <Button
        style={{ width: "fit-content" }}
        variant="contained"
        color="primary"
        type="submit"
        className="my-3"
      >
        Check Balance
      </Button>
    </FormComponent>
  );
};

export default BalanceComponent;
