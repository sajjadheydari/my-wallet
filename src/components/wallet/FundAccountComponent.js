import React from "react";
import AddFundComponent from "./AddFundComponent";
import BalanceComponent from "./BalanceComponent";
import { CircularProgress, Grid } from "@mui/material";
import { useSelector } from "react-redux";

const FundAccountComponent = () => {
  const {loading} = useSelector(state => state.wallet)
  return (
    <>
      {loading ? (
         <CircularProgress style={{position: 'fixed', top:'50%' , left: '50%'}}/>
      ) : (
        <Grid container className="d-flex justify-content-start">
          <Grid item xs={12} sm={11} md={5} lg={4} className="mb-4">
            <AddFundComponent />
          </Grid>
          <Grid item xs={12} sm={11} md={7} lg={5}>
            <BalanceComponent />
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default FundAccountComponent;
