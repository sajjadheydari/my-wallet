import React, { useState } from "react";
import { Alert, Button, CircularProgress, Grid } from "@mui/material";
import { useSelector } from "react-redux";
import FormComponent from "../FormComponent";
import InputComponent from "../InputComponent";
import PropTypes from "prop-types";

const ConnectNetworkComponent = ({ handleConnectNetwork, loading }) => {
  const [network, setNetwork] = useState({});
  const messages = useSelector((state) => state.messages);

  const handleSubmit = (event) => {
    event.preventDefault();
    handleConnectNetwork(network);
  };

  const handleChange = (val) => {
    setNetwork(val);
  };

  const selectOptions = [
    {
      value: "https://api.devnet.solana.com",
      label: "DevNet",
    },
    {
      value: "https://api.testnet.solana.com",
      label: "TestNet",
    },
  ];

  const validated =
    network === undefined || Object.keys(network).length === 0 ? true : false;

  return (
    <>
      {loading ? (
        <CircularProgress
          style={{ position: "fixed", top: "50%", left: "50%" }}
        />
      ) : (
        <Grid container className="d-flex justify-content-start">
          <Grid item xs={12} sm={6} md={5} lg={4} className="mb-4">
            <FormComponent
              title="Connect to Solana Network:"
              handleSubmit={handleSubmit}
            >
              {messages.connectNetwork.length === 0 ||
              messages.connectNetwork === undefined ? null : (
                <Alert
                  severity={
                    messages.connectNetwork.id === 1 ? "success" : "error"
                  }
                  className={messages.connectNetwork.id === 1 ? 'bg-success' : 'bg-danger'}
                >
                  {messages.connectNetwork.text}
                </Alert>
              )}
              <InputComponent
                size="small"
                variant="outlined"
                onChange={handleChange}
                value={network}
                selectOptions={selectOptions}
                className="my-4 input-component"
                label="Choose a Network"
                select={true}
                InputLabelProps={{
                  shrink: true,
                  className: "input-label-props",
                }}
              />
              <Button
                style={{ width: "fit-content" }}
                variant="contained"
                color="primary"
                type="submit"
                className="my-3"
                disabled={validated}
              >
                Connect
              </Button>
            </FormComponent>
          </Grid>
        </Grid>
      )}
    </>
  );
};

ConnectNetworkComponent.propTypes = {
  handleConnectNetwork: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default ConnectNetworkComponent;
