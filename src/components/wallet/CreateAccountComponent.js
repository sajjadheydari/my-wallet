import { Alert, Button, CircularProgress, Grid } from "@mui/material";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { generateKeypair } from "../../redux/actions/walletActions";
import FormComponent from "../FormComponent";
import InputComponent from "../InputComponent";
import PropTypes from "prop-types";

const CreateAccountComponent = ({ loading }) => {
  const { connectedNetwork, walletAddress } = useSelector(
    (state) => state.wallet
  );
  const messages = useSelector((state) => state.messages);
  const { address } = walletAddress;
  const dispatch = useDispatch();

  const handleGenerateKeypair = (e) => {
    e.preventDefault();
    if (connectedNetwork === undefined || connectedNetwork.length === 0) {
      alert("Please connect to Network first!");
    } else {
      dispatch(generateKeypair());
    }
  };
  return (
    <>
      {loading ? (
        <CircularProgress
          style={{ position: "fixed", top: "50%", left: "50%" }}
        />
      ) : (
        <>
          <Grid container className="d-flex justify-content-start">
            <Grid item xs={12} sm={9} md={9} lg={5}>
              <FormComponent
                title="Your New Account Address:"
                handleSubmit={handleGenerateKeypair}
              >
                {messages.createAccount.length === 0 ||
                messages.createAccount === undefined ? null : (
                  <Alert severity={messages.createAccount.id === 1 ? 'success' : 'error'} className={messages.createAccount.id === 1 ? 'bg-success' : 'bg-danger'}>
                    {messages.createAccount.text}
                  </Alert>
                )}
                <InputComponent
                  size="small"
                  variant="outlined"
                  label="Your PubKey"
                  type="text"
                  className="my-4 input-component"
                  value={address}
                  name="payer"
                  inputProps={{
                    readOnly: true,
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: "input-label-props",
                  }}
                />
                <Button
                  style={{ width: "fit-content" }}
                  variant="contained"
                  color="primary"
                  type="submit"
                  className="my-3"
                >
                  Create Account
                </Button>
              </FormComponent>
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
};

CreateAccountComponent.prototype = {
  loading: PropTypes.bool.isRequired,
};

export default CreateAccountComponent;
