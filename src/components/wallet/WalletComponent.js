import { Grid } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import VerticalTabs from "../VerticalTabComponent";
import ConnectNetworkComponent from "./ConnectNetworkComponent";
import CreateAccountComponent from "./CreateAccountComponent";
import FundAccountComponent from "./FundAccountComponent";
import TransferComponent from "./TransferComponent";
import { connectNetwork } from "../../redux/actions/walletActions";
import PropTypes from "prop-types";

const WalletScreen = ({ isOpen }) => {
  const { connectedNetwork, loading } = useSelector((state) => state.wallet);
  const dispatch = useDispatch();

  const handleConnectNetwork = (network) => {
    dispatch(connectNetwork(network));
  };

  const tabs = [
    {
      tabName: "Connect to Solana",
      content: (
        <ConnectNetworkComponent
          handleConnectNetwork={handleConnectNetwork}
          loading={loading}
        />
      ),
    },
    {
      tabName: "Create an account",
      content: <CreateAccountComponent loading={loading} />,
    },
    {
      tabName: "Fund your wallet",
      content: <FundAccountComponent />,
    },
    {
      tabName: "Transfer SOL",
      content: <TransferComponent />,
    },
  ];

  return (
    <Grid
      container
      className={
        " walletscreen " +
        (isOpen ? "walletscreen-shrink" : "walletscreen-expand")
      }
    >
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <div
          className={
            "text-center " +
            (connectedNetwork === undefined || connectedNetwork.length === 0
              ? "connection-failed"
              : "connection-success")
          }
        >
          {connectedNetwork === undefined || connectedNetwork.length === 0
            ? "Not Connected"
            : `You are connected to ${connectedNetwork.type} (Version: ${connectedNetwork.version})`}{" "}
        </div>
        <VerticalTabs tabs={tabs}> </VerticalTabs>
      </Grid>
    </Grid>
  );
};

WalletScreen.propTypes = {
  isOpen: PropTypes.bool.isRequired,
};

export default WalletScreen;
