import React, { useState } from "react";
import FormComponent from "../FormComponent";
import InputComponent from "../InputComponent";
import { Button } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { fundAccount } from "../../redux/actions/walletActions";
import { Alert } from "@mui/material";

const AddFundComponent = () => {
  const [SOL, setSOL] = useState([]);
  const { connectedNetwork, walletAddress, hashKey } = useSelector(
    (state) => state.wallet
  );
  const messages = useSelector((state) => state.messages);
  const { address } = walletAddress;
  const dispatch = useDispatch();

  const handleFundAccount = (e) => {
    e.preventDefault();
    if (connectedNetwork === undefined || connectedNetwork.length === 0) {
      alert("Please connect to Network first!");
    } else if (address === undefined || address.length === 0) {
      alert("Please create/connect account first!");
    } else {
      dispatch(fundAccount(connectedNetwork.network, address, SOL));
    }
  };

  const validated = SOL === undefined || SOL.length === 0 ? true : false;

  return (
    <FormComponent title="Fund your account:" handleSubmit={handleFundAccount}>
      {messages.fundAccount.length === 0 ||
      messages.fundAccount === undefined ? null : (
        <Alert
          severity={messages.fundAccount.id === 1 ? "success" : "error"}
          className={messages.fundAccount.id === 1 ? "bg-success" : "bg-danger"}
        >
          {messages.fundAccount.text}
        </Alert>
      )}
      <InputComponent
        size="small"
        variant="outlined"
        label="Enter Lamport amount"
        type="number"
        className="my-4 input-component"
        value={SOL || ''}
        onChange={setSOL}
        name="SOL"
        InputLabelProps={{
          shrink: true,
          className: "input-label-props",
        }}
      />
      {hashKey === undefined || hashKey.length === 0 ? null : (
        <InputComponent
          size="small"
          variant="outlined"
          label="Get your HashKey"
          type="text"
          className="my-4 input-component"
          value={hashKey.fundHash}
          name="hashKey"
          inputProps={{
            readOnly: true,
          }}
          InputLabelProps={{
            shrink: true,
            className: "input-label-props",
          }}
        />
      )}
      <Button
        style={{ width: "fit-content" }}
        variant="contained"
        color="primary"
        type="submit"
        className="my-3"
        disabled={validated}
      >
        Fund My Account
      </Button>
    </FormComponent>
  );
};

export default AddFundComponent;
