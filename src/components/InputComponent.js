import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import { TextField } from "@mui/material";
import PropTypes from "prop-types";

const InputComponent = ({
  onChange,
  classes,
  value,
  name,
  selectOptions,
  placeholder,
  label,
  select,
  size,
  variant,
  multiline,
  inputProps,
  ...rest
}) => {
  const handleChange = (event, index, value) => {
    onChange(event.target.value);
  };

  return (
    <>
      <TextField
        classes={classes}
        size={size}
        label={label}
        variant={variant}
        select={select}
        value={value || ''}
        name={name}
        placeholder={placeholder}
        multiline={multiline}
        onChange={handleChange}
        inputProps={inputProps}
        {...rest}
      >
        {selectOptions &&
          selectOptions.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
      </TextField>
    </>
  );
};

InputComponent.propTypes = {
  onChange: PropTypes.func,
  classes: PropTypes.object,
  value: PropTypes.any,
  name: PropTypes.string,
  selectOptions: PropTypes.array,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  select: PropTypes.bool,
  size: PropTypes.string,
  variant: PropTypes.string.isRequired,
  multiline: PropTypes.bool,
  inputProps: PropTypes.object,
};

export default InputComponent;
