import React from "react";
import { Paper } from "@material-ui/core";
import FormHeaderComponent from "./FormHeaderComponent";
import FormBodyComponent from "./FormBodyComponent";
import PropTypes from "prop-types";

const FormComponent = ({ handleSubmit, title, children, ...rest }) => {
  return (
    <Paper style={{ padding: 16, marginRight: 10 }}>
      <form onSubmit={handleSubmit} noValidate {...rest}>
        <FormHeaderComponent title={title} />
        <FormBodyComponent>{children}</FormBodyComponent>
      </form>
    </Paper>
  );
};

FormComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default FormComponent;
