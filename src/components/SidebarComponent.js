import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";

const SidebarComponent = ({ menuItems, onToggle, isOpen }) => {
  return (
    <Grid>
      <button
        className={"sidebar-btn " + (!isOpen ? "sidebar-btn-close" : "sidebar-btn-open")}
        onClick={onToggle}
      >
        <i className="fas fa-bars" />
      </button>

      <nav
        className={"sidebar " + (!isOpen ? "sidebar-close" : "sidebar-open")}
      >
        <div className="title">Side Menu</div>
        <ul className="list-items">
          {menuItems &&
            menuItems.map((item, index) => {
              return (
                <li key={index}>
                  <Link to={item.path}>
                    {item.icon}
                    {item.title}
                  </Link>
                </li>
              );
            })}

          <div className="icons">
            <Link to="#">
              <i className="fab fa-facebook-f" />
            </Link>
            <Link to="#">
              <i className="fab fa-twitter" />
            </Link>
            <Link to="#">
              <i className="fab fa-github" />
            </Link>
            <Link to="#">
              <i className="fab fa-youtube" />
            </Link>
          </div>
        </ul>
      </nav>
    </Grid>
  );
};

SidebarComponent.propTypes = {
  menuItems: PropTypes.array.isRequired,
  onToggle: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default SidebarComponent;
