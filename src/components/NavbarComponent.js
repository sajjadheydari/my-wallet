import { Grid } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import Logo from "../assets/images/logo.png";

const NavbarComponent = () => {
  return (
    <Grid container className="navbar">
      <Grid item>
          <img
            className="navbar__logo"
            src={Logo}
            alt="logo"
            width="50px"
            height="45px"
          />
      </Grid>
      <Grid item>
        <ul className="navbar-ul text-white">
          <Link to="/dashboard">
            <li style={{fontWeight: "bolder"}}>Dashboard</li>
          </Link>
        </ul>
      </Grid>
    </Grid>
  );
};

export default NavbarComponent;
