import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import LandingScreen from "./screens/LandingScreen";
import NotfoundScreen from "./screens/NotfoundScreen";
import DashboardScreen from "./screens/DashboardScreen";
import { useEffect, useState } from "react";
import store from "../src/redux/store";
import { Provider } from "react-redux";
import { CssBaseline, Grid } from "@mui/material";

function App() {
  const [isLoading, setLoading] = useState(true);

  function fakeRequest() {
    return new Promise((resolve) => setTimeout(() => resolve(), 2000));
  }

  useEffect(() => {
    fakeRequest().then(() => {
      const el = document.querySelector(".loader-container");
      if (el) {
        el.remove();
        setLoading(!isLoading);
      }
    });
  }, [isLoading]);

  if (isLoading) {
    return null;
  }

  return (
    <Provider store={store}>
      <Router>
        <Grid container>
        <CssBaseline />
          <Switch>
            <Route path="/" component={LandingScreen} exact />
            <Route path="/dashboard" component={DashboardScreen} exact />
            <Route component={NotfoundScreen} />
          </Switch>
        </Grid>
      </Router>
    </Provider>
  );
}

export default App;
