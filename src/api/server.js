const solanaWeb3 = require("@solana/web3.js");
const express = require("express");
const app = express();
const port = 8000;

app.use(express.json());

// Connect to Solana Network
app.post("/", async (req, res) => {
  try {
    const { network } = req.body;
    const netType =
      network === "https://api.devnet.solana.com" ? "DevNet" : "TestNet";
    const connection = new solanaWeb3.Connection(network, "confirmed");
    const version = await connection.getVersion();
    res.status(200).json({
      type: netType,
      network: network,
      version: version["solana-core"],
    });
  } catch (error) {
    res.status(500).json(error.message);
  }
});

// Create an account (keypair)
app.post("/generate", async (req, res) => {
  try {
    const keypair = solanaWeb3.Keypair.generate();
    const address = keypair?.publicKey.toString();
    const secret = JSON.stringify(Array.from(keypair.secretKey));
    res.status(200).json({
      secret,
      address,
    });
  } catch (error) {
    res.status(500).json(error.message);
  }
});

// Fund an account
app.post("/fund", async (req, res) => {
  try {
    const { network, address, SOL } = req.body;
    const connection = new solanaWeb3.Connection(network, "confirmed");
    const publicKey = new solanaWeb3.PublicKey(address);
    const hash = await connection.requestAirdrop(publicKey, parseInt(SOL));
    await connection.confirmTransaction(hash);
    res.status(200).json(hash);
  } catch (error) {
    res.status(500).json(error);
  }
});

// Get Ballance
app.post("/balance", async (req, res) => {
  try {
    const { network, address } = req.body;
    const connection = new solanaWeb3.Connection(network, "confirmed");
    const publicKey = new solanaWeb3.PublicKey(address);
    const balance = await connection.getBalance(publicKey);
    if (balance === undefined) {
      throw new Error("Account not funded");
    }
    res.status(200).json(balance);
  } catch (error) {
    res.status(500).json(error.message);
  }
});

// Transfer SOL
app.post("/transfer", async (req, res) => {
  try {
    const { address, secret, recipient, SOL, network } = req.body;
    const connection = new solanaWeb3.Connection(network, "confirmed");
    const fromPubkey = new solanaWeb3.PublicKey(address);
    const toPubkey = new solanaWeb3.PublicKey(recipient);
    const secretKey = Uint8Array.from(JSON.parse(secret));

    const balance = await connection.getBalance(fromPubkey);
    if (balance === undefined) {
      throw new Error("Invalid fromPubkey address");
    } else if (balance === 0 || balance < SOL) {
      throw new Error("Not enough balance!");
    }

    const instructions = solanaWeb3.SystemProgram.transfer({
      fromPubkey,
      toPubkey,
      SOL,
    });

    const signers = [
      {
        publicKey: fromPubkey,
        secretKey,
      },
    ];

    const transaction = new solanaWeb3.Transaction().add(instructions);

    const hash = await solanaWeb3.sendAndConfirmTransaction(
      connection,
      transaction,
      signers
    );

    res.status(200).json(hash);
  } catch (error) {
    console.log(error);
    res.status(500).json(error.message);
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
